





# travel

> A Vue.js project

## 用vue写去哪儿网 ---移动端

## *项目介绍*：

数据：完全来自于json数据-----用axios接收数据渲染 

```
import axios from 'axios'

 methods:{
      getDtailInfo(){
          axios.get('/api/detail.json?' ,{
              params:{
                    id: this.$route.params.id
              }
          }).then(this.handleGetDateSucc)
      },
      handleGetDateSucc(res){
             res=res.data
             if(res.ret && res.data){
                const data=res.data
                this.sightName=data.sightName
                this.bannerImg=data.bannerImg
                this.gallaryImgs=data.gallaryImgs
                this.categoryList=data.categoryList
             }
      }
  },
```

![主页](https://images.gitee.com/uploads/images/2019/0531/134835_fa987f96_2297434.png)
![城市选择](https://images.gitee.com/uploads/images/2019/0531/134835_b434990f_2297434.png)
![筛选](https://images.gitee.com/uploads/images/2019/0531/134840_c855d9fb_2297434.png)
![详情页面](https://images.gitee.com/uploads/images/2019/0531/134840_3e485eff_2297434.png)
![详情页面-轮播](https://images.gitee.com/uploads/images/2019/0531/134835_c0e13d88_2297434.png)

For a detailed explanation on how things work, check out the [guide](http://vuejs-templates.github.io/webpack/) and [docs for vue-loader](http://vuejs.github.io/vue-loader).